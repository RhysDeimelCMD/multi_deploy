# Test multi account deploy
A simple demo that extends the default cdk workshop example to deploy SQS in multiple accounts


## How To
First, bootstrap any new accounts:
```
# For the management account. This is the root CI account that all others trust
env CDK_NEW_BOOTSTRAP=1 cdk bootstrap \
	--profile np-mgmt \
	--cloudformation-execution-policies arn:aws:iam::aws:policy/AdministratorAccess \
	aws://164797267917/ap-southeast-2

# For all other accounts. --trust is the account ID of the management account
env CDK_NEW_BOOTSTRAP=1 cdk bootstrap \
	--profile <named_profile> \
	--cloudformation-execution-policies arn:aws:iam::aws:policy/AdministratorAccess \
	--trust 164797267917 \
	aws://<account ID>/ap-southeast-2
```
Then, you'll have to add the `cdk-assume-role-credential-plugin` to your `cdk.json`. Edit `cdk.json` and add the following line:
```json
"plugin": ["cdk-assume-role-credential-plugin"]
```

Next up, you need to tell the plugin which roles to assume as it iterates through accounts. In your `cdk.json`, append the following entries to the `context` block:
```json
"context": {
  "assume-role-credentials:readIamRoleName": "cdk-hnb659fds-deploy-role-{ACCOUNT_ID}-ap-southeast-2",
  "assume-role-credentials:writeIamRoleName": "cdk-hnb659fds-deploy-role-{ACCOUNT_ID}-ap-southeast-2"
}
```

In your `bin/filename.ts`, instantiate a new stack using an [environment](https://docs.aws.amazon.com/cdk/latest/guide/environments.html). An environment is a combination of the target AWS account and AWS region into which an individual stack is intended to be deployed.

A CDK app can contain multiple stacks the are deployed to multiple environments. A simple example of this would be an application that deployed a `dev` stack into a `dev` AWS account and a `prod` stack into a `prod` AWS account. This would look something like:

```
const dev  = { account: '2383838383', region: 'us-east-2' };
const prod = { account: '8373873873', region: 'us-east-2' };

new MyAppStack(app, 'dev', { env: dev });
new MyAppStack(app, 'prod', { env: prod });
```

When you run a cdk command such as synth or deploy the cli will need to perform actions against the AWS account that is defined for the stack.

_Note: Because the management account is the trusted CI account, all `cdk` commands should be run with the management profile._

## Outputs
Application:
```
$ cdk deploy --profile np-mgmt --require-approval never --all

AuditTestStack
AuditTestStack: deploying...
[0%] start: Publishing b040c413380da13c71bc27e4c6d509c7f114e88b9bd71c1e0eb1682cce8d0b05:576390211876-ap-southeast-2
[100%] success: Published b040c413380da13c71bc27e4c6d509c7f114e88b9bd71c1e0eb1682cce8d0b05:576390211876-ap-southeast-2
AuditTestStack: creating CloudFormation changeset...
[██████████████████████████████████████████████████████████] (6/6)






 ✅  AuditTestStack

Stack ARN:
arn:aws:cloudformation:ap-southeast-2:576390211876:stack/AuditTestStack/dcfc1520-757a-11eb-a7b5-027d8657c1ec
MgmtTestStack
MgmtTestStack: deploying...
[0%] start: Publishing fb7cfabfb94439cd4e37a953f4b0b36b9114e0e633972ab2c0884d75132bf702:164797267917-ap-southeast-2
[100%] success: Published fb7cfabfb94439cd4e37a953f4b0b36b9114e0e633972ab2c0884d75132bf702:164797267917-ap-southeast-2
MgmtTestStack: creating CloudFormation changeset...
[██████████████████████████████████████████████████████████] (6/6)




 ✅  MgmtTestStack

Stack ARN:
arn:aws:cloudformation:ap-southeast-2:164797267917:stack/MgmtTestStack/ed04d970-757a-11eb-a3ef-0ae6120c515c
NonprodTestStack
NonprodTestStack: deploying...
[0%] start: Publishing bde9089deee4624943ce4dac3e15d40e3fab868c63e11930ac113d9bac961c93:828986065666-ap-southeast-2
[100%] success: Published bde9089deee4624943ce4dac3e15d40e3fab868c63e11930ac113d9bac961c93:828986065666-ap-southeast-2
NonprodTestStack: creating CloudFormation changeset...
[██████████████████████████████████████████████████████████] (6/6)






 ✅  NonprodTestStack

Stack ARN:
arn:aws:cloudformation:ap-southeast-2:828986065666:stack/NonprodTestStack/fd4094a0-757a-11eb-8597-0ae3d5cf3704
ProdTestStack
ProdTestStack: deploying...
[0%] start: Publishing 6c0d142ef636702d659b29fda135ae128e287c7fb67ea51cdf73338493ae839f:576390211876-ap-southeast-2
[100%] success: Published 6c0d142ef636702d659b29fda135ae128e287c7fb67ea51cdf73338493ae839f:576390211876-ap-southeast-2
ProdTestStack: creating CloudFormation changeset...
[██████████████████████████████████████████████████████████] (6/6)






 ✅  ProdTestStack

Stack ARN:
arn:aws:cloudformation:ap-southeast-2:576390211876:stack/ProdTestStack/0d5d0800-757b-11eb-a0d4-062f4cabc2b4

```

Destruction:
```
$ cdk destroy --profile np-mgmt --all --force

ProdTestStack: destroying...
12:53:53 pm | DELETE_IN_PROGRESS   | AWS::CloudFormation::Stack | ProdTestStack
12:53:56 pm | DELETE_IN_PROGRESS   | AWS::SQS::Queue        | TestQueue

 ✅  ProdTestStack: destroyed
NonprodTestStack: destroying...
12:55:00 pm | DELETE_IN_PROGRESS   | AWS::CloudFormation::Stack | NonprodTestStack
12:55:04 pm | DELETE_IN_PROGRESS   | AWS::SQS::Queue        | TestQueue

 ✅  NonprodTestStack: destroyed
MgmtTestStack: destroying...
12:56:06 pm | DELETE_IN_PROGRESS   | AWS::CloudFormation::Stack | MgmtTestStack
12:56:09 pm | DELETE_IN_PROGRESS   | AWS::SQS::Queue        | TestQueue

 ✅  MgmtTestStack: destroyed
AuditTestStack: destroying...
12:57:13 pm | DELETE_IN_PROGRESS   | AWS::CloudFormation::Stack | AuditTestStack



 ✅  AuditTestStack: destroyed

```

## Resources
[Plugin page](https://github.com/aws-samples/cdk-assume-role-credential-plugin)
[CDK Environment bootstrapping](https://docs.aws.amazon.com/cdk/api/latest/docs/pipelines-readme.html#cdk-environment-bootstrapping)