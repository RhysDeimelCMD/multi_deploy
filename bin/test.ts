#!/usr/bin/env node
import * as cdk from '@aws-cdk/core';
import { TestStack } from '../lib/test-stack';

const audit = {account: '576390211876', region: 'ap-southeast-2'};
const mgmt = {account: '164797267917', region: 'ap-southeast-2'};
const prod = {account: '576390211876', region: 'ap-southeast-2'};
const nonprod = {account: '828986065666', region: 'ap-southeast-2'};

const app = new cdk.App();

new TestStack(app, 'AuditTestStack', { env: audit });
new TestStack(app, 'ProdTestStack', { env: prod });
new TestStack(app, 'MgmtTestStack', { env: mgmt });
new TestStack(app, 'NonprodTestStack', { env: nonprod });